const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 20,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    createdOn: {
        type: Date,
        default: Date.now(),
    },
});

// custom mongoose error on same username
// eslint-disable-next-line func-names, prefer-arrow-callback
userSchema.post('save', function (error, doc, next) {
    if (error.name === 'MongoError' && error.code === 11000) {
        next('Username is already taken');
    } else {
        next(error);
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;
