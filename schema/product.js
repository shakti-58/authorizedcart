const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 30,
        // unique: true,
    },
    price: {
        type: Number,
        required: true,
        min: [1, 'Price cannot be smaller than 1.'],
    },
    description: {
        type: String,
        required: true,
        minlength: 10,
        maxlength: 50,
    },
    category: {
        type: String,
        enum: {
            values: ['soap', 'lotion', 'cream'],
            message: 'category is either soap, lotion or cream',
        },
        required: [true, 'Please mention the category of the product.'],
    },
    createdOn: {
        type: Date,
        default: Date.now(),
    },
});

// custom error for unique product name
// eslint-disable-next-line prefer-arrow-callback
productSchema.post('save', function (error, doc, next) {
    if (error.name === 'MongoError' && error.code === 11000) {
        next('Product name already exists.');
    } else {
        next(error);
    }
});

const Product = mongoose.model('Product', productSchema);
module.exports = Product;
