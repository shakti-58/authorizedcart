const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// schema
const User = require('../schema/auth');
const validateUser = require('../validations/auth');

const router = express.Router();

// function that returns a newly generated token when called upon
const createToken = (user, secret, expiresIn) => {
    const { username, email } = user;
    return jwt.sign({ email, username }, secret, { expiresIn });
};

// login existing user
router.post('/login', async (req, res) => {
    const { body } = req;

    // validate user login credentials
    const {
        username,
        password: userLoginPassword,
    } = body;

    // const { error } = validateUser('login', body);

    // // user login validation error
    // if (error) return res.status(400).send(error);

    // find the user in the data base
    const user = await User.findOne({ username });

    // if user does not exists
    if (!user) return res.status(400).send('No such user exists.');

    const {
        _id,
        password: userRealPassword,
    } = user;

    // validate the user password
    const isPasswordValid = await bcrypt.compare(userLoginPassword, userRealPassword);

    // throw error if password is not valid
    if (!isPasswordValid) return res.status(400).send('Password did not match.');

    // create the token ( expires in 1 hour )
    const token = createToken(user, process.env.JWT_SECRET_KEY, '1h');

    // return token and user _id in response
    return res.status(200).send({
        _id,
        token,
    });
});

// registering a new user
router.post('/register', async (req, res) => {
    const { body } = req;

    // validate user login credentials
    const {
        username,
        email,
        password,
    } = body;

    const { error } = validateUser('register', body);

    // user registration validation error
    if (error) return res.status(400).send(error.message);

    // hash the password
    const hashPassword = await bcrypt.hash(password, 12);

    const newUser = User({
        username,
        email,
        password: hashPassword,
    });

    // save user to data base
    try {
        const result = await newUser.save();
        const { _id } = result;

        // create the token ( expires in 1 hour )
        const token = createToken(result, process.env.JWT_SECRET_KEY, '1h');

        // return token and user _id in response
        return res.status(200).send({ _id, token });
    } catch (jwtError) {
        return res.status(400).send(jwtError);
    }
});

// validating token for protected routes for frontend
router.post('/token', async (req, res) => {
    const { body: { token } } = req;

    // if no token provided through an error
    if (!token) return res.status(400).send('Access denied.');

    try {
        // if token is present get user from the token
        const currentUser = await jwt.verify(token, process.env.JWT_SECRET_KEY);

        // throw error in case got no user information from JWT
        if (!currentUser) return res.status(400).send('Access denied.');
        const { username } = currentUser;

        // find the user in data base
        const user = await User.findOne({ username });

        // throw error if user not found
        if (!user) return res.status(400).send('Invalid token.');
        req.currentUser = currentUser;
        return res.status(400).send('success');
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;
