const express = require('express');

const router = express.Router();

// schemas
const addProductSchema = require('../validations/product');
const Product = require('../schema/product');

// middleware
const verifyToken = require('../middleware/token');

// to add product
router.post('/addProduct', verifyToken, async (req, res) => {
    const { body } = req;
    const { error } = addProductSchema(body);

    // product fails the validation throw an error
    if (error) return res.status(400).send(error.message);

    // create product object
    const addProduct = Product({ ...body });

    // save product to data base
    try {
        await addProduct.save();
        return res.status(200).send('Product successfully added to database.');
    } catch (mongooseError) {
        return res.status(400).send(mongooseError);
    }
});

// to get product
router.get('/getProduct', verifyToken, async (req, res) => {
    const { query: { id: _id } } = req;

    const searchQuery = _id ? { _id } : {};
    try {
        const product = await Product.find(searchQuery);
        return res.status(200).send(product);
    } catch (mongooseError) {
        return res.status(400).send(mongooseError);
    }
});

// to update product
router.post('/updateProduct', verifyToken, async (req, res) => {
    const { body } = req;
    const { id, ...remainingKeys } = body;

    const { error } = addProductSchema({ ...remainingKeys });

    // if product fails the validation throw an error
    if (error) return res.status(400).send(error.message);

    if (!id) return res.status(400).send('id was not provided.');

    try {
        const product = await Product.findById(id);
        product.set({ ...remainingKeys });

        await product.save();
        return res.status(200).send('Product details updated successfully.');
    } catch (mongooseError) {
        return res.status(400).send(mongooseError);
    }
});

module.exports = router;
