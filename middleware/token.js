const jwt = require('jsonwebtoken');
const User = require('../schema/auth');

// user authorization verification
const token = async (req, res, next) => {
    // get the token from auth header
    const bearerToken = req.headers.authorization?.split(' ')[1] || null;

    // if no token provided through an error
    if (!bearerToken) return res.status(400).send('Access denied.');

    try {
        // if token is present get user from the token
        const currentUser = await jwt.verify(bearerToken, process.env.JWT_SECRET_KEY);

        // throw error in case got no user information from JWT
        if (!currentUser) return res.status(400).send('Access denied.');
        const { username } = currentUser;

        // find the user in data base
        const user = await User.findOne({ username });

        // throw error if user not found
        if (!user) return res.status(400).send('Invalid token.');
        req.currentUser = currentUser;
        return next();
    } catch (error) {
        return res.status(400).send(error);
    }
};

module.exports = token;
