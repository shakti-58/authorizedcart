const Joi = require('joi');

// custom error messages
const errorMessages = {
    confirmPassword: {
        'any.only': '"password" did not match.',
    },
};

// function to apply custom error messages accordingly
const customErrorFunction = (errors) => (
    errors.map((error) => {
        const { code, path } = error;
        if (path[0] === 'confirmPassword') {
            // eslint-disable-next-line no-param-reassign
            error.message = errorMessages[path][code];
        }
        return error;
    })
);

// user login schema
const authSchema = {
    register: Joi.object().keys({
        username: Joi.string().alphanum().required().min(5)
            .max(30),
        email: Joi.string().email().required(),
        password: Joi.string().required().min(8).max(30),
        confirmPassword: Joi.string().valid(Joi.ref('password')).required()
            .error((errors) => customErrorFunction(errors)),
    }),
    login: Joi.object().keys({
        username: Joi.string().required().min(5).max(30),
        password: Joi.string().required().min(8).max(30),
    }),
};

// get the required schema and validate the request body
const validateUser = (type, body) => authSchema[type].validate(body);

module.exports = validateUser;
