const Joi = require('joi');

// custom error messages
const errorMessages = {
    name: {
        'string.base': '"name" should be of type string.',
        'string.required': '"name" is a required field.',
        'string.max': '"name" must be under 30 characters.',
    },
    price: {
        'number.base': '"price" should be of type number.',
        'number.required': '"price" is a required field.',
        'number.min': '"price" must be greater than 0.',
    },
    description: {
        'string.base': '"description" should be of type string.',
        'string.required': '"description" is a required field.',
        'string.min': '"description" must be at least 10 character.',
        'string.max': '"description" must be under 50 characters.',
    },
    category: {
        'any.only': '"category" should be either soap, lotion or cream.',
    },
};

// function to apply custom error messages accordingly
const customErrorFunction = (errors) => (
    errors.map((error) => {
        const { code, path } = error;
        // eslint-disable-next-line no-param-reassign
        error.message = errorMessages[path][code];
        return error;
    })
);

// product validation schema
const addProductSchema = Joi.object().keys({
    name: Joi.string().required().max(30)
        .error((errors) => customErrorFunction(errors)),
    price: Joi.number().required().min(1)
        .error((errors) => customErrorFunction(errors)),
    description: Joi.string().required().min(10).max(50)
        .error((errors) => customErrorFunction(errors)),
    category: Joi.string().required().valid('soap', 'lotion', 'cream')
        .error((errors) => customErrorFunction(errors)),
});

// validate body against schema
const validateProduct = (body) => addProductSchema.validate(body);

module.exports = validateProduct;
