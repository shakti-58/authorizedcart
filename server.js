const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

const product = require('./routes/product');
const auth = require('./routes/auth');

const app = express();

const corsOptions = {
    origin: 'http://localhost:3000',
    credential: true,
};

// cross cors
app.use(cors(corsOptions));

// JSON parser middleware
app.use(express.json());

// routes
app.use('/auth', auth);
app.use('/cart', product);

// mongoose connection
mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
})
    .then(() => {
        console.log('connection is established with mongodb');
    })
    .catch((err) => {
        console.log(`Couldn't connect to mongodb ${err}`);
    });

const { env: { PORT } } = process;

// server port
app.listen(
    PORT,
    () => {
        console.log(`server running on port: ${PORT}`);
    },
);
